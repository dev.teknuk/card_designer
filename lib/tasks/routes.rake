# frozen_string_literal: true

# annotate --routes failed https://github.com/ctran/annotate_models/issues/983
task routes: :environment do
  puts `bundle exec rails routes`
end
