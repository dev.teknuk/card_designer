# frozen_string_literal: true

source 'https://rubygems.org'

ruby '3.3.0'

# Bundle edge Rails instead: gem "rails", github: "rails/rails", branch: "main"
gem 'rails', '~> 7.1.3', '>= 7.1.3.2'

# The original asset pipeline for Rails [https://github.com/rails/sprockets-rails]
gem 'sprockets-rails'

# Use postgresql as the database for Active Record
gem 'pg', '~> 1.1'

# Use the Puma web server [https://github.com/puma/puma]
gem 'puma', '>= 5.0'

# Bundle and transpile JavaScript [https://github.com/rails/jsbundling-rails]
gem 'jsbundling-rails'

# Hotwire's SPA-like page accelerator [https://turbo.hotwired.dev]
gem 'turbo-rails'

# Hotwire's modest JavaScript framework [https://stimulus.hotwired.dev]
gem 'stimulus-rails'

# Bundle and process CSS [https://github.com/rails/cssbundling-rails]
gem 'cssbundling-rails'

# Use Redis adapter to run Action Cable in production
# gem "redis", ">= 4.0.1"

# Use Kredis to get higher-level data types in Redis [https://github.com/rails/kredis]
# gem "kredis"

# Use Active Model has_secure_password [https://guides.rubyonrails.org/active_model_basics.html#securepassword]
# gem "bcrypt", "~> 3.1.7"

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[windows jruby]

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', require: false

# Use Active Storage variants [https://guides.rubyonrails.org/active_storage_overview.html#transforming-images]
# gem "image_processing", "~> 1.2"

group :development, :test do
  # See https://guides.rubyonrails.org/debugging_rails_applications.html#debugging-with-the-debug-gem
  gem 'debug', platforms: %i[mri windows]

  # Testing Framework
  gem 'factory_bot_rails' # factory_bot as fixtures replacement [https://thoughtbot.github.io/factory_bot/]
  gem 'rspec-rails', '~> 6.1.0' # rspec-rails brings the RSpec BDD testing framework to Ruby on Rails [https://github.com/rspec/rspec-rails]
end

group :development do
  # Use console on exceptions pages [https://github.com/rails/web-console]
  gem 'web-console'

  # Add speed badges [https://github.com/MiniProfiler/rack-mini-profiler]
  # gem "rack-mini-profiler"

  # Speed up commands on slow machines / big apps [https://github.com/rails/spring]
  # gem "spring"

  # Code analysis and formatting
  gem 'annotate' # Add a comment summarizing the current schema to models, fixtures, and tests [https://github.com/ctran/annotate_models]
  gem 'rubocop', require: false # Ruby static code analyzer and formatter [https://github.com/rubocop/rubocop]
  gem 'rubocop-performance', require: false # RuboCop extension for Performance optimization analysis [https://github.com/rubocop/rubocop-performance]
  gem 'rubocop-rails', require: false # RuboCop extension for Rails [https://github.com/rubocop/rubocop-rails]
  gem 'rubocop-rspec', require: false # Code style checking for RSpec files [https://github.com/rubocop/rubocop-rspec]
  # gem 'rubocop-factory_bot', require: false # dependency_of rubocop-rspec # Code style checking for factory_bot files [https://github.com/rubocop/rubocop-factory_bot]
  # gem 'rubocop-capybara', require: false # dependency_of rubocop-rspec # Code style checking for Capybara files [https://github.com/rubocop/rubocop-capybara]
end

group :test do
  # Testing
  gem 'capybara' # Acceptance test framework for web applications [https://github.com/teamcapybara/capybara]
  gem 'capybara-screenshot' # Automatically save screen shots when a Capybara scenario fails [https://github.com/mattheworiordan/capybara-screenshot]
  gem 'selenium-webdriver' # Ruby bindings for Selenium [https://github.com/SeleniumHQ/selenium/tree/trunk/rb]
  gem 'shoulda-matchers', '~> 6.0' # Simple one-liner tests for common Rails functionality [https://github.com/thoughtbot/shoulda-matchers]
end
